package mx.com.ca;

import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import mx.com.aion.motor.webservice.OperacionNacionalServicePortType;


public class App {
	private static Logger LOGGER = LoggerFactory.getLogger(App.class);
	
	public static void main(String[] args) {
		((App) new App()).does();
	}

	private void does() {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "clientBeans.xml" });

		OperacionNacionalServicePortType client = (OperacionNacionalServicePortType) context.getBean("testService");

		LOGGER.debug("CXF 2.7.");
		LOGGER.debug("Creating service.");
		LOGGER.debug("Invoking service method.");

		try {
			String doOperacionActualizaPago = client.doOperacionActualizaPago("asdasd", "asdfasdf", new Integer(234), "SASDF");
			LOGGER.debug("Respuesta: " + doOperacionActualizaPago);
		} catch(SOAPFaultException e) {
			LOGGER.error("Error invoking method.", e);
		} finally {
			context.close();
		}
		
	}
	
}