
package mx.com.aion.motor.cxf;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.com.aion.motor.cxf package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DoOperacionPagoCashIn_QNAME = new QName("http://cxf.motor.aion.com.mx/", "doOperacionPagoCashIn");
    private final static QName _DoOperacionPagoCashInResponse_QNAME = new QName("http://cxf.motor.aion.com.mx/", "doOperacionPagoCashInResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mx.com.aion.motor.cxf
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DoOperacionPagoCashIn }
     * 
     */
    public DoOperacionPagoCashIn createDoOperacionPagoCashIn() {
        return new DoOperacionPagoCashIn();
    }

    /**
     * Create an instance of {@link mx.com.aion.motor.cxf.Pago }
     * 
     */
    public mx.com.aion.motor.cxf.Pago createPago() {
        return new mx.com.aion.motor.cxf.Pago();
    }

    /**
     * Create an instance of {@link DoOperacionPagoCashInResponse }
     * 
     */
    public DoOperacionPagoCashInResponse createDoOperacionPagoCashInResponse() {
        return new DoOperacionPagoCashInResponse();
    }

    /**
     * Create an instance of {@link DoOperacionPagoCashIn.Pago }
     * 
     */
    public DoOperacionPagoCashIn.Pago createDoOperacionPagoCashInPago() {
        return new DoOperacionPagoCashIn.Pago();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoOperacionPagoCashIn }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cxf.motor.aion.com.mx/", name = "doOperacionPagoCashIn")
    public JAXBElement<DoOperacionPagoCashIn> createDoOperacionPagoCashIn(DoOperacionPagoCashIn value) {
        return new JAXBElement<DoOperacionPagoCashIn>(_DoOperacionPagoCashIn_QNAME, DoOperacionPagoCashIn.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoOperacionPagoCashInResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://cxf.motor.aion.com.mx/", name = "doOperacionPagoCashInResponse")
    public JAXBElement<DoOperacionPagoCashInResponse> createDoOperacionPagoCashInResponse(DoOperacionPagoCashInResponse value) {
        return new JAXBElement<DoOperacionPagoCashInResponse>(_DoOperacionPagoCashInResponse_QNAME, DoOperacionPagoCashInResponse.class, null, value);
    }

}
