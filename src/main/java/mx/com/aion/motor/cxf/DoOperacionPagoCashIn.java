
package mx.com.aion.motor.cxf;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for doOperacionPagoCashIn complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="doOperacionPagoCashIn"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="pago" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="idSistema" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *                   &lt;element name="inputData" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doOperacionPagoCashIn", propOrder = {
    "pago"
})
public class DoOperacionPagoCashIn {

    protected DoOperacionPagoCashIn.Pago pago;

    /**
     * Gets the value of the pago property.
     * 
     * @return
     *     possible object is
     *     {@link DoOperacionPagoCashIn.Pago }
     *     
     */
    public DoOperacionPagoCashIn.Pago getPago() {
        return pago;
    }

    /**
     * Sets the value of the pago property.
     * 
     * @param value
     *     allowed object is
     *     {@link DoOperacionPagoCashIn.Pago }
     *     
     */
    public void setPago(DoOperacionPagoCashIn.Pago value) {
        this.pago = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="idSistema" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
     *         &lt;element name="inputData" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idSistema",
        "inputData"
    })
    public static class Pago {

        protected int idSistema;
        @XmlElement(required = true)
        protected String inputData;

        /**
         * Gets the value of the idSistema property.
         * 
         */
        public int getIdSistema() {
            return idSistema;
        }

        /**
         * Sets the value of the idSistema property.
         * 
         */
        public void setIdSistema(int value) {
            this.idSistema = value;
        }

        /**
         * Gets the value of the inputData property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInputData() {
            return inputData;
        }

        /**
         * Sets the value of the inputData property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInputData(String value) {
            this.inputData = value;
        }

    }

}
