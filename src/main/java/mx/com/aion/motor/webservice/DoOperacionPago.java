
package mx.com.aion.motor.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import mx.com.aion.motor.cxf.Pago;


/**
 * <p>Java class for doOperacionPago complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="doOperacionPago"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://cxf.motor.aion.com.mx/}Pago" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doOperacionPago", propOrder = {
    "pago"
})
public class DoOperacionPago {

    @XmlElement(name = "Pago", namespace = "http://cxf.motor.aion.com.mx/")
    protected Pago pago;

    /**
     * Gets the value of the pago property.
     * 
     * @return
     *     possible object is
     *     {@link Pago }
     *     
     */
    public Pago getPago() {
        return pago;
    }

    /**
     * Sets the value of the pago property.
     * 
     * @param value
     *     allowed object is
     *     {@link Pago }
     *     
     */
    public void setPago(Pago value) {
        this.pago = value;
    }

}
